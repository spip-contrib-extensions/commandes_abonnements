<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-commandes_abonnements?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_choisir_offre' => 'このプランを選ぶ',

	// C
	'champ_montant_minimum_label' => '最小額',
	'champ_montant_perso_label' => '法人向け合計',
	'champ_montant_perso_label_case' => '法人向け合計額を表示',
	'champ_renouvellement_auto_label' => '更新',
	'champ_renouvellement_auto_label_case' => '自動更新にする',

	// E
	'explication_renouvellement' => 'この定期購読プランを既にご登録の場合は、その期限からさらに延長されます。',

	// I
	'info_dates_from' => '@date_debut@から',
	'info_dates_from_to' => '@date_debut@から@date_fin@まで',
	'info_duree' => '期間',
	'info_paiement' => '支払い',
	'info_paiement_auto' => '支払い方法（期間）',
	'info_paiement_renouvelable' => 'ユーロ圏自動支払い',
	'info_paiement_unique' => '唯一の',
	'info_renouvellement_auto' => '自動更新'
);

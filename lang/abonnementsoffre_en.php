<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-commandes_abonnements?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_choisir_offre' => 'Choose this subscription',

	// C
	'champ_montant_minimum_label' => 'Min amount',
	'champ_montant_perso_label' => 'Custom amount',
	'champ_montant_perso_label_case' => 'Enable custom amount',
	'champ_montant_perso_visiteur_label' => 'Customize the amount',
	'champ_montant_perso_visiteur_placeholder' => 'Other',
	'champ_renouvellement_auto_label' => 'Renewal',
	'champ_renouvellement_auto_label_case' => 'Enable automatic renewal',

	// E
	'explication_renouvellement' => 'If you already have an active subscription to this offer, when you log in, it will be extended from its end date.',

	// I
	'info_dates_from' => 'From @date_debut@',
	'info_dates_from_to' => 'From @date_debut@ to @date_fin@',
	'info_duree' => 'Duration:',
	'info_paiement' => 'Payment',
	'info_paiement_auto' => 'Direct Debit frequency',
	'info_paiement_renouvelable' => '16 / 5 000
Résultats de traduction
SEPA Direct Debit',
	'info_paiement_unique' => 'Unique',
	'info_renouvellement_auto' => 'Automatic renewal'
);

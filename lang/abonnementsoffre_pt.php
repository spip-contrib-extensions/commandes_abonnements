<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-commandes_abonnements?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_choisir_offre' => 'Escolher esta opção',

	// C
	'champ_montant_minimum_label' => 'Montante mínimo',
	'champ_montant_perso_label' => 'Montante personalizado',
	'champ_montant_perso_label_case' => 'Activar a personalização do montante',
	'champ_renouvellement_auto_label' => 'Renovação',
	'champ_renouvellement_auto_label_case' => 'Activar a renovação automática',

	// E
	'explication_renouvellement' => 'Se já tiver uma modalidade de assinatura activa, ao iniciar sessão, esta será renovada a partir da sua data final.',

	// I
	'info_dates_from' => 'A partir de @date_debut@',
	'info_dates_from_to' => 'De @date_debut@ a @date_fin@',
	'info_duree' => 'Duração',
	'info_paiement' => 'Pagamento',
	'info_paiement_auto' => 'Periodicidade do pagamento',
	'info_paiement_renouvelable' => 'Transferência SEPA',
	'info_paiement_unique' => 'Único',
	'info_renouvellement_auto' => 'Renovação automática'
);

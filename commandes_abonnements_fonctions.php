<?php

function filtre_abonnement_test_renouvellement_auto($id_abonnement) {
    static $renouvellement_auto = [];
    if ($abonnement = sql_fetsel('id_abonnement, id_abonnements_offre, ending, statut', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement))) {
        if (empty($abonnement['ending']) && in_array($abonnement['statut'], ['prepa', 'actif'])) {
            if (!isset($renouvellement_auto[$abonnement['id_abonnements_offre']])) {
                $renouvellement_auto[$abonnement['id_abonnements_offre']] = sql_getfetsel('renouvellement_auto', 'spip_abonnements_offres', 'id_abonnements_offre='.intval($abonnement['id_abonnements_offre']));
            }
            return $renouvellement_auto[$abonnement['id_abonnements_offre']] ? ' ' : '';
        }
    }
    return '';
}